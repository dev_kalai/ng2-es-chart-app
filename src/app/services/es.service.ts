import { Injectable } from '@angular/core';
import { Client } from 'elasticsearch';

@Injectable()
export class EsService {
  private _client: Client;

  constructor() {
    if (!this._client) {
      this._connect();
    }
   }

  private defer() {
    let resolve, reject, promise;

    promise = new Promise((_resolve, _reject) => {
      resolve = _resolve;
      reject = _reject;
    });

    return { resolve, reject, promise };
  }
  
  private _connect() {
     this._client = new Client({
       host: 'http://localhost:9200',
       log: 'trace',
       defer: this.defer
     });
   }


  isAvailable() {
     return this._client.ping({requestTimeout: Infinity});
   }

  public SingleYearResult(year: number) {
    return this._client.get({
      index: 'election',
      type: 'tn',
      id: year
    });
  }

  public multiYearResult(years: number[]) {
    return this._client.mget({
      index: 'election',
      type: 'tn',
      body: {
        ids: years
      }
    });
  }

}
