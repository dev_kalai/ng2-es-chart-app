import { Component } from '@angular/core';
import { EsService } from './services/es.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EsService]
})
export class AppComponent {
  title = 'TN Assembly Elections Result';
}
