import { Component, OnInit } from '@angular/core';
import { EsService } from '../services/es.service';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css']
  // providers: [EsService]
})
export class ChatsComponent implements OnInit {

  private singleYear: number;
  private singleYearResultLable: string[];
  private singleYearResultData;
  private showCharts: boolean = false;

  // bar-chart-var
  barChartOptions: any;
  barChartLabels: string[];
  barChartType: string;
  barChartLegend: boolean;
  barChartData: any[];

  // Doughnut
  doughnutChartLabels: string[];
  doughnutChartData: number[];
  doughnutChartType: string;

  constructor(private _esservice: EsService) { }

  ngOnInit() {
    this.getSingleYearResult(2016);
    // this.getAllYearResults([1991, 1996, 2001, 2006, 2011, 2016]);
  }

  // EsService Functions
    private getSingleYearResult(year: number): void {
      this._esservice.SingleYearResult(year)
          .then(result => {
            const data = result._source;
            this.singleYearResultLable = Object.keys(data.seats);
            this.singleYearResultData = Object.values(data.seats);
            console.log(this.singleYearResultLable);
            console.log(this.singleYearResultData);
            
            this.drawBarChart();
            this.drawDoughnutChart();
            this.showCharts = true;
            this.singleYear = year;
          });
    }

    private getAllYearResults(years: number[]) {
      this._esservice.multiYearResult(years).then(response => {
        // get only found result
        const results = response.docs.filter(result => result.found === true);
      });
    }

    private toggleChart(): void {
      this.barChartType = this.barChartType === 'line' ? 'bar' : 'line';
      this.doughnutChartType = this.doughnutChartType === 'doughnut' ? 'pie' : 'doughnut';
    }

  // Chart functions
    public drawBarChart(): void {
      this.barChartType = 'bar';
      this.barChartLegend = false;
      this.barChartLabels = this.singleYearResultLable;
      this.barChartData = [ { data: this.singleYearResultData } ];
      this.barChartOptions = { scaleShowVerticalLines: false, responsive: true };
    }

    public drawDoughnutChart(): void {
      this.doughnutChartType = 'doughnut';
      this.doughnutChartLabels = this.singleYearResultLable;
      this.doughnutChartData = this.singleYearResultData;
    }



}
